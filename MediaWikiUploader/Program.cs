﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using CommandLine;
// ReSharper disable ClassNeverInstantiated.Local

namespace MediaWikiUploader
{
    class Program
    {
        class BaseApiOptions
        {
            [Option("api", Required = true, 
                HelpText = "URL path to a wikimedia API.php, example: http://my.wiki.url/api.php")]
             public string ApiPath { get; set; }

            [Option('u', "username", Required = true,
                HelpText = "Wiki username")]
            public string Username { get; set; }

            [Option('p', "password", Required = true,
                HelpText = "Wiki user password")]
            public string Password { get; set; }
        }

        [Verb("upload", HelpText = "Upload scanned 26.04 documents")]
        class UploadOptions : BaseApiOptions
        {
            [Option('f', "folder", Required = true,
               HelpText = "Folder to inspect")]
            public string Folder { get; set; }

            [Option("pattern", Required = false,
               HelpText = "Folder to inspect", Default = "*.*")]
            public string SearchPattern { get; set; }
        }

        [Verb("extlinks", HelpText = "Create and article for scanned 26.04 documents with links to external storage")]
        class ExternalLinksOptions : BaseApiOptions
        {
            [Option('f', "folder", Required = true,
               HelpText = "Folder to inspect")]
            public string Folder { get; set; }

            [Option('s', "storage", Required = true,
               HelpText = "Folder to inspect")]
            public string ExternalStorage { get; set; }

            [Option("pattern", Required = false,
               HelpText = "Folder to inspect", Default = "*.*")]
            public string SearchPattern { get; set; }
        }

        [Verb("download", HelpText = "Downloads an article as plain text to specified folder")]
        class DownloadOptions : BaseApiOptions
        {
            [Option('a', "article", Required = true,
               HelpText = "Wiki article name")]
            public string Article { get; set; }

            [Option('f', "folder", Required = true,
               HelpText = "Target folder")]
            public string Folder { get; set; }
        }

        [Verb("category", HelpText = "Clone a repository into a new directory.")]
        class CategoryOptions : BaseApiOptions
        {
            [Option('a', "article", Required = true,
               HelpText = "Wiki article name")]
            public string Article { get; set; }

            [Option('c', "category", Required = true,
               HelpText = "Wiki category name")]
            public string CategoryName { get; set; }

            [Option("remove", Required = false, Default = false,
               HelpText = "If set category will be removed from article")]
            public bool Remove { get; set; }
        }

        static int Main(string[] args)
        {
            var res = Parser.Default.ParseArguments<UploadOptions, DownloadOptions, CategoryOptions, ExternalLinksOptions>(args)
                .MapResult(
                  (UploadOptions opts) => Upload(opts),
                  (DownloadOptions opts) => Download(opts),
                  (CategoryOptions opts) => Category(opts),
                  (ExternalLinksOptions opts) => ExternalLinks(opts),
                  errs => 1);
            return res;
        }

        public static string[] ImageExtensions = {".jpg", ".png", ".gif"};

        private static int Upload(UploadOptions options)
        {
            using (var wiki = new WikiApiInterface(options.ApiPath))
            {
                try
                {
                    wiki.Login(options.Username, options.Password);
                }
                catch (Exception e)
                {
                    LogError(e.Message);
                    return 1;
                }   

                LogInfo($"\n\nLooking into {options.Folder}");

                var foldersLogs = new List<FolderUploadLogEntry>();
                var root = new DirectoryInfo(options.Folder);
                var dirs = root.GetDirectories();

                foreach (var dir in dirs)
                    LogTrace($"Found directory:\t{dir.Name}");

                var parser = new DirNameParser();
                
                foreach (var dir in dirs)
                {
                    LogInfo($"\n\n\nLooking into {dir.Name}");
                    var imageGalleryMarkup = new StringBuilder("<gallery>");
                    var docsMarkup = new StringBuilder();

                    var res = parser.Parse(dir.Name);
                    LogTrace($"Code:\t{res.Code}");
                    LogTrace($"Date:\t{res.Date.ToShortDateString()}");
                    LogTrace($"Title:\t{res.Title}");
                    LogTrace($"New Title:\t{res.ToWikiTitle()}");

                    var files = dir.GetFiles(options.SearchPattern);
                    var fileIndex = 1;

                    var folderLog = new FolderUploadLogEntry()
                    {
                        OriginalFolder = dir.FullName,
                        WikiPage = res.ToWikiTitle()
                    };

                    foreach (var file in files)
                    {
                        var wikiFileName = $"{res.Year}_{res.Code}_{file.Name}";

                        if(IsImage(file))
                            wikiFileName = $"{res.Year}_{res.Code}_Scanimage{fileIndex++:000}{file.Extension}";

                        LogInfo($"\nUploading {file.Name} to {wikiFileName}");

                        var fileUlpoadRes = wiki.UploadFile(file, wikiFileName);

                        if (fileUlpoadRes.error != null)
                        {
                            LogError(fileUlpoadRes.error.ToString());
                            continue;
                        }

                        if (fileUlpoadRes.upload?.result?.Value.Equals("Warning"))
                        {
                            if (fileUlpoadRes.upload?.warnings?.duplicate != null)
                            {
                                var duplicate = fileUlpoadRes.upload?.warnings?.duplicate[0];
                                if (duplicate != null)
                                {
                                    wikiFileName = duplicate.Value;
                                }
                                else
                                {
                                    LogError("Can't upload");
                                    continue;
                                }
                            }
                        }

                        folderLog.Files.Add(new FileUploadLogEntry()
                        {
                            OriginalFile = file.Name,
                            WikiFile = wikiFileName
                        });
                        if (IsImage(file))
                            imageGalleryMarkup.AppendLine($"File:{wikiFileName}");
                        else
                            docsMarkup.AppendLine($"* [[File:{wikiFileName}]]");
                    }
                    foldersLogs.Add(folderLog);

                    
                    imageGalleryMarkup.Append("</gallery>\n");

                    imageGalleryMarkup.Append(docsMarkup);
                    LogTrace($"\nDocs:\n{imageGalleryMarkup}");

                    LogInfo($"\nSubmitting page \"{res.ToWikiTitle()}\"");
                    wiki.UpdateArticle(res.ToWikiTitle(), imageGalleryMarkup.ToString(), summary: "Files upload");
                }

                LogInfo($"\n\nPublishing upload log");

                var logSection = new StringBuilder();

                foreach (var folderUploadLogEntry in foldersLogs)
                {
                    logSection.AppendLine($"=== [[{folderUploadLogEntry.WikiPage}]] ===");
                    logSection.AppendLine($"* Source: {folderUploadLogEntry.OriginalFolder}");
                    logSection.AppendLine($"* Date: {folderUploadLogEntry.DateTime}");
                    logSection.AppendLine($"\nFiles:");
                    logSection.AppendLine("<gallery mode=\"traditional\">");
                    foreach (var file in folderUploadLogEntry.Files)
                    {
                        logSection.AppendLine($"File:{file.WikiFile}| Src: {file.OriginalFile}");
                    }
                    logSection.AppendLine("</gallery>");
                }

                wiki.UpdateArticle("ApiLog", logSection.ToString(), section:"new", summary: DateTime.Now.ToString("R"));
            }

            return 0;
        }

        private static int ExternalLinks(ExternalLinksOptions options)
        {
            using (var wiki = new WikiApiInterface(options.ApiPath))
            {
                try
                {
                    wiki.Login(options.Username, options.Password);
                }
                catch (Exception e)
                {
                    LogError(e.Message);
                    return 1;
                }

                LogInfo($"\n\nLooking into {options.Folder}");

                var foldersLogs = new List<FolderUploadLogEntry>();
                var root = new DirectoryInfo(options.Folder);
                var dirs = root.GetDirectories();

                foreach (var dir in dirs)
                    LogTrace($"Found directory:\t{dir.Name}");

                var parser = new DirNameParser();

                foreach (var dir in dirs)
                {
                    LogInfo($"\n\n\nLooking into {dir.Name}");

                    var docsMarkup = new StringBuilder();

                    var res = parser.Parse(dir.Name);
                    LogTrace($"Code:\t{res.Code}");
                    LogTrace($"Date:\t{res.Date.ToShortDateString()}");
                    LogTrace($"Title:\t{res.Title}");
                    LogTrace($"New Title:\t{res.ToWikiTitle()}");

                    var files = dir.GetFiles(options.SearchPattern);

                    var folderLog = new FolderUploadLogEntry()
                    {
                        OriginalFolder = dir.FullName,
                        WikiPage = res.ToWikiTitle()
                    };

                    foreach (var file in files)
                    {
                        LogInfo($"\nAdding {file.Name}");

                        folderLog.Files.Add(new FileUploadLogEntry()
                        {
                            OriginalFile = file.Name,
                            WikiFile = file.Name
                        });

                        var url = new Uri(options.ExternalStorage + file.Name);
                        docsMarkup.AppendLine($"* [{url.AbsoluteUri} {file.Name}]");
                    }
                    foldersLogs.Add(folderLog);


                    LogTrace($"\nDocs:\n{docsMarkup}");

                    LogInfo($"\nSubmitting page \"{res.ToWikiTitle()}\"");
                    wiki.UpdateArticle(res.ToWikiTitle(), docsMarkup.ToString(), summary: "Files upload");
                }

                LogInfo($"\n\nPublishing upload log");

                var logSection = new StringBuilder();

                foreach (var folderUploadLogEntry in foldersLogs)
                {
                    logSection.AppendLine($"=== [[{folderUploadLogEntry.WikiPage}]] ===");
                    logSection.AppendLine($"* Source: {folderUploadLogEntry.OriginalFolder}");
                    logSection.AppendLine($"* Date: {folderUploadLogEntry.DateTime}");
                    logSection.AppendLine($"\nFiles:");
                    foreach (var file in folderUploadLogEntry.Files)
                    {
                        var url = new Uri(options.ExternalStorage + file.WikiFile);
                        logSection.AppendLine($"* Doc: [{url.AbsoluteUri} {file.WikiFile}]");
                    }
                }

                wiki.UpdateArticle("ApiLog", logSection.ToString(), section: "new", summary: DateTime.Now.ToString("R"));
            }

            return 0;
        }

        private static int Download(DownloadOptions options)
        {
            LogInfo($"Downloading \"{options.Article}\" to \"{options.Folder}\"");
            using (var api = new WikiApiInterface(options.ApiPath))
            {
                var filePath = Path.Combine(options.Folder, options.Article + ".txt");
                try
                {
                    api.SaveArticleToFile(options.Article, filePath);
                }
                catch (Exception e)
                {
                    LogError($"Can't download article {e.Message}");
                    return 0;
                }
                LogInfo("Done. Exiting");
                return 0;
            }
        }

        private static int Category(CategoryOptions options)
        {
            using (var wiki = new WikiApiInterface(options.ApiPath))
            {
                LogInfo($"Downloading \"{options.Article}\"");
                string content;
                try
                {
                    wiki.Login(options.Username, options.Password);
                    content = wiki.LoadArticle(options.Article);
                }
                catch (Exception e)
                {
                    LogError($"Can't load article {e.Message}. Exiting");
                    return 1;
                }

                string newcontent;
                var categoryRegex = new Regex(@"\[\[(?:Category|Категория):([^\]\[]*)\]\]");
                var matches = categoryRegex.Matches(content);
                var foundCategories = new List<string>();
                foreach (Match match in matches)
                {
                    var cat = match.Groups[1].Value.Trim();
                    foundCategories.Add(cat);
                    LogTrace($"Found category: \"{cat}\"");
                }

                if (options.Remove)
                {
                    LogInfo($"Trying to remove category \"{options.CategoryName}\" from \"{options.Article}\"");
                    var found = false;
                    newcontent = categoryRegex.Replace(content, match =>
                    {
                        if (match.Groups[1].Value.Trim().Equals(options.CategoryName))
                        {
                            LogTrace("Category removed from page markup");
                            found = true;
                            return String.Empty;
                        }
                        return match.Value;
                    });
                    if (!found)
                    {
                        LogError("Requested category not found. Exiting");
                        return 1;
                    }
                }
                else
                {
                    if (foundCategories.Contains(options.CategoryName))
                    {
                        LogError($"Category \"{options.CategoryName}\" alregy set in page \"{options.Article}\". Exiting");
                        return 1;
                    }
                    LogInfo($"Trying to add category \"{options.CategoryName}\" to \"{options.Article}\"");
                    var sb = new StringBuilder(content);
                    sb.AppendLine($"[[Category:{options.CategoryName}]]");
                    newcontent = sb.ToString();
                }
           
                if (!string.IsNullOrEmpty(newcontent))
                {
                    LogInfo($"Submitting changes");
                   
                    try
                    {
                        wiki.UpdateArticle(options.Article, newcontent, summary: "Updated category");
                        LogInfo($"Done. Exiting");
                    }
                    catch (Exception e)
                    {
                        LogError(e.Message);
                        return 1;
                    }
                }

                return 0;
            }
        }

        private static bool IsImage(FileInfo info)
        {
            return ImageExtensions.Contains(info.Extension);
        }

        private static void LogError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private static void LogTrace(string message)
        {
            Console.ResetColor();
            Console.WriteLine(message);
        }

        private static void LogInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
