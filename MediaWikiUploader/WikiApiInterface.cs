﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MediaWikiUploader
{
    class WikiApiInterface : IDisposable
    {
        private readonly HttpClient _client;
        public string ApiPath { get; }
        public string IndexPath => ApiPath.Replace("api.php", "index.php");
        public string LoginToken { get; private set; }
        public string CsrfToken { get; private set; }

        public WikiApiInterface(string apiApiPath)
        {
            ApiPath = apiApiPath;
            
            var handler = new HttpClientHandler { CookieContainer = new CookieContainer() };
            _client = new HttpClient(handler);
        }

        public void Login(string userName, string password)
        {
            var loginResult = Post("login", new MultipartFormDataContent()
                {
                    {new StringContent(userName), "lgname"},
                    {new StringContent(password), "lgpassword"},
                }).Result;

            var loginToken = loginResult.login.token.Value;
            if (loginToken == null)
            {
                throw new ApplicationException("Cannot obtain login token with this creditentials");
            }
            LoginToken = loginToken;

            var confirmRes = Post("login", new MultipartFormDataContent()
                {
                    {new StringContent(userName), "lgname"},
                    {new StringContent(password), "lgpassword"},
                    {new StringContent(loginToken), "lgtoken"},
                }).Result;

            if (confirmRes.login.result.Value == null || confirmRes.login.result.Value != "Success")
            {
                throw new ApplicationException($"Failed to login. {confirmRes.login.result?.Value}");
            }
            
            var csrfTokenRes = Post("query", new MultipartFormDataContent()
                {
                    {new StringContent("tokens"), "meta"},
                }).Result;

            var csrfToken = csrfTokenRes.query.tokens.csrftoken.Value;
            if (string.IsNullOrEmpty(csrfToken))
            {
                throw new ApplicationException("Can't obtain csrf token");
            }
            CsrfToken = csrfToken;
        }

        public async Task<dynamic> Post(string action, MultipartFormDataContent payload)
        {
            payload.Add(new StringContent(action), "action");
            payload.Add(new StringContent("json"), "format");
            var response = await _client.PostAsync(ApiPath, payload);
            response.EnsureSuccessStatusCode();
            var res = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject(res);
        }

        public async Task<string> Get(string action)
        {
            var response = await _client.GetAsync(action);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        public string LoadArticle(string name)
        {
            return Get(IndexPath + $"?title={name}&action=raw").Result;
        }

        public dynamic UpdateArticle(string name, string text, string section = "0", string summary = "Updated via api")
        {
            if(string.IsNullOrEmpty(CsrfToken))
                throw new InvalidOperationException("Csrftoken required. Use login first");

            return Post("edit", new MultipartFormDataContent()
            {
                {new StringContent(name), "title"},
                {new StringContent("0"), "section"},
                {new StringContent("Updating page category"), "summary"},
                {new StringContent(text), "text"},
                {new StringContent(CsrfToken), "token"},
            }).Result;
        }

        public void SaveArticleToFile(string name, string fileName)
        {
            using (var writer = new StreamWriter(File.Open(fileName, FileMode.OpenOrCreate)))
            {
                writer.Write(Get(IndexPath + $"?title={name}&action=raw").Result);
            }
        }

        public dynamic UploadFile(FileInfo file, string wikiFileName = null)
        {
            if (wikiFileName == null)
                wikiFileName = file.Name;

            return Post("upload", new MultipartFormDataContent()
            {
                { new StringContent(wikiFileName), "filename" },
                { new StringContent($"Uploaded using file-uploader script. Source: {file.FullName}"), "comment" },
                { new ByteArrayContent(File.ReadAllBytes(file.FullName)), "file", wikiFileName},
                {new StringContent(CsrfToken), "token"},
            }).Result;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
