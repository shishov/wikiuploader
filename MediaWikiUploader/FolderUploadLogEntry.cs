using System;
using System.Collections.Generic;

namespace MediaWikiUploader
{
    class FolderUploadLogEntry
    {
        public DateTime DateTime = DateTime.Now;
        public string OriginalFolder;
        public string WikiPage;
        public List<FileUploadLogEntry> Files = new List<FileUploadLogEntry>();
    }
}