namespace MediaWikiUploader
{
    class FileUploadLogEntry
    {
        public string OriginalFile;
        public string WikiFile;
    }
}