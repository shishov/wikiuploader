﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MediaWikiUploader
{
    class DirNameParser
    {
        private readonly Regex _regex;

        public class DirNameParseResult
        {
            public string Code { get; set; }
            public string Title { get; set; }
            public int Year { get; set; }
            public int Month { get; set; }
            public int Day { get; set; }
            public bool YearOnly { get; set; }

            public DateTime Date
            {
                get
                {
                    if (YearOnly)
                    {
                        return new DateTime(Year);
                    }
                    
                    return new DateTime(Year, Month, Day);
                }
            }

            

            public string ToWikiTitle()
            {
                if (YearOnly)
                    return $"{Title} от {Year}г";

                return $"{Title} от {Date.ToShortDateString()}";
            }
        }

        public DirNameParser()
        {
            _regex = new Regex(@"^(?<code>\d+\([^\)]+\))(?:\s|_)(?<year>\d+)(?:\-(?<month>\d+)\-(?<day>\d+))?\s?\((?<title>[^\)]+)\)$");
        }

        public DirNameParseResult Parse(string fileName)
        {
            var groups = _regex.Match(fileName).Groups;

            var year = Convert.ToInt32(groups["year"].Value);
            if (year <= DateTime.Now.Year - 2000)
                year += 2000;
            else if (year < 100)
                year += 1900;

            var result = new DirNameParseResult()
            {
                Title = groups["title"].Value,
                Code = groups["code"].Value,
                Year = year
            };


            if (groups["month"].Success && groups["day"].Success)
            {
                result.Month = Convert.ToInt32(groups["month"].Value);
                result.Day = Convert.ToInt32(groups["day"].Value);
                result.YearOnly = false;
            }
            else
            {
                result.YearOnly = true;
            }

            return result;
        }
    }
}
